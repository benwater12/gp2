package hotelmain;

import java.time.LocalDate;
import java.time.format.DateTimeParseException;
import java.util.ArrayList;
import java.util.Random;
import pet.*;
import java.text.DecimalFormat;
/**
 * @author B05505033 Benson Weng
 * @since 2018/06/09
 * @version 1.1
 <h1>This class represents a room for a pet to live and record all its activeites.</h1>
 <p>Change Log<br>
 0.1: Generated this class<br>
 0.2:the class is halfway done, and added some commits<br>
 0.3: added a check method which tells the room is full or not.<br>
 1.0 :all fired up and ready to serve~<br>
 1.1fixed a shit bug which cases the system to crash.<br>
 */
public class Room {
	public class Datebefore extends Exception {
		private static final long serialVersionUID = 1L;
	}

	private Pet pet;
	LocalDate leave;//as said it's a date to leave
	LocalDate arrive;//as said it's a date to come here
	private	ArrayList<Incident> events;
	String ID;// custermer ID it's ramdom...

	public Room() {
		pet = null;
		leave = null;
		arrive = null;
		events = null;
		ID = null;
	}

	public Room(Pet pet, String leave, Date arrive) {
		this.pet = pet;
		this.arrive = Date.toLocal(arrive);
		try {
			this.leave = LocalDate.parse(leave, Date.getformat());
			if (this.leave.isBefore(this.arrive)) {// in case that a "leaves faster than arrive happens"
				throw new Datebefore();
			}

		} catch (DateTimeParseException a) {
			System.out.println("Error taking default date as 3 days later");
			this.leave = this.arrive.plusDays(3);
		} catch (Datebefore e) {
			System.out.println("Error leave earlier than arrive... taking default date as 3 days later");
			this.leave = this.arrive.plusDays(3);
		}
		Random IDgen = new Random();
		long temp = IDgen.nextInt();
		if (temp < 0) {
			temp = temp * -1;
		}
		DecimalFormat db=new DecimalFormat("0000000000");
		events = new ArrayList<Incident>();
		ID = db.format(temp);
		System.out.println("Your ID :" + ID);
	}

	/**
	 * this is a ramdom generated ID as a number
	 * 
	 * @return The ID (Deep copy).
	 */
	public String getID() {
		return new String(ID);
	}

	/**
	 * this room is empty or not
	 * 
	 * @return boolean checks the pet space
	 */
      boolean checkvacant() {
		if (pet == null) {
			return true;
		}
		return false;
	}

	/**
	 * gives the array of notice
	 * 
	 * @return the original notice array not a new one!!!
	 */
	public	ArrayList<String> getnotice() {
		return pet.getnotice();
	}

	/**
	 * gives the array of no eating
	 * 
	 * @return the original noeat array not a new one!!!
	 */
	public	ArrayList<String> getnoeat() {
		return pet.getnoeat();
	}


	/**
	 * get all the events happened in this list
	 * 
	 * @return An Array Incident regardless of the date.
	 * returns and emptylist if there is nothing in the List.
	 */
	public ArrayList<Incident> getincidentlist() {
		ArrayList<Incident> temp = new ArrayList<Incident>();
		for (int i = 0; i <events.size(); i++) {
			 
				temp.add(events.get(i));
			
		}
		return temp;
	}

	/**
	 * get all the incident with the matching date.
	 * 
	 * @param happen
	 *            input the date wanted
	 * @return if none match is found return an empty list, returns Array Incident
	 *         considering of the date.
	 */
	public ArrayList<Incident> getincidentlist(Date happen) {
		ArrayList<Incident> temp = new ArrayList<Incident>();
		for (int i = 0; i <events.size(); i++) {
			if (Date.Stringinform(happen).equals(Date.Stringinform(events.get(i).getdate()))) {
				temp.add(events.get(i));
			}
		}
		return temp;
	}

	/**
	 * Get the leaving date
	 * 
	 * @return the leaving date, or checkout date!
	 */
	public	Date getleavedate() {
		Date temp=new Date(leave.minusDays(0));
		return temp;
	}

	/**
	 * changes the leaving date in case for an extend stay or change default.
	 * 
	 * @param a
	 *            string in yyyy-MM-dd
	 */
	public	void setleavedate(String a) {
		try {
			if (LocalDate.parse(a, Date.getformat()).isBefore(arrive)) {
				throw new Datebefore();
			}
			this.leave = LocalDate.parse(a, Date.getformat());
			System.out.println(Date.getformat().format(leave));
		} catch (DateTimeParseException ac) {
			System.out.println("Error not in yyyy-MM-dd");
		} catch (Datebefore e) {

			System.out.println("The date is before arrive date.");
		}

	}

	/**
	 * this adds a incident to the list of events
	 */
	public void addincident(String event, int price, Date a) {
		Incident e = new Incident(event, price, a);
		events.add(e);
	}

	/**
	 * Get the arrving date
	 * 
	 * @return the arraival date, or checkin date!
	 */
	public	Date getarrivaldate() {
		Date temp=new Date(arrive.minusDays(0));
	return temp;
	}

	/**
	 * 
	 * @return get a string which contains the services which are turned on
	 */
	public	String activeservices() {
		return pet.getactive();
	}

	/**
	 * 
	 * @param a
	 *            order ID(case sensitive)
	 * @return true=the same, false no or compair is not correct
	 */
	boolean match(String a) {
	return ID.equals(a);
	}

	/**
	 * @param a
	 *            the Pet class which contains the name...(case sensitive)
	 * @return true=the same, false no or compair is not correct
	 */
	boolean matchpet(String a) {
		return pet.getname().equals(a);
	}

	/**
	 * @param a
	 *            matching the master is the same person or not.(case sensitive)
	 * @return true=the same, false no or compair is not correct
	 */
	boolean matchmaster(String a) {
		return pet.compair(a);
	}
	/**
	 * this help the class to calulate the price
	 * @return
	 * int[0],Incedent cost...int[1], ONE DAY SERVICE price!!!, int[2], ONE DAY STAY PRICE.
	 */
	public int[] getprice() {
		int temp[]=new int[3];
		for(int i=0;i<events.size();i++)
		{temp[0]=temp[0]+events.get(i).getprice();}
		temp[1]=pet.getprice();
		temp[2]=2000;
		return temp;
	}
	/**
	 * This method will return the pet's information of the room.
	 * @return A String object that stores the pet's information.
	 */
	public String getPetInfo() {
		String petInfo = "";
		ArrayList<String> notEat = pet.getnoeat(),notices = pet.getnotice();
		petInfo += "Your name:"+pet.getMaster()+"\n";
		petInfo += "Your pet's name:"+pet.getname()+"\n";
		petInfo += "Your pet's birthday:"+pet.getBitrthday()+"\n";
		if(!(notEat.isEmpty()||notEat.equals(null))) {
			petInfo += "Food could not eat:\n";
			for(int i=0;i<notEat.size();i++) {
				petInfo += (i+1)+"."+notEat.get(i)+"\n";
			}
		}
		if(!(notices.isEmpty()||notices.equals(null))) {
			petInfo += "Notices:\n";
			for(int i=0;i<notices.size();i++) {
				petInfo += (i+1)+"."+notices.get(i)+"\n";
			}
		}
		return petInfo;
	}
	/**
	 * Testing!
	 * @param a Some parameters.
	 */
	public static void main(String[] a) {
		Services[] a1= {Services.lightbulb,Services.Bettertoys};
		Room a11 = new Room(new Lizard( "12",  "12",  "12",a1  ), "2102-10-10",new Date());
		a11.addincident("SICK", 20000,new Date());
		a11.addincident("SICK", 10000,new Date("2104-12-12"));
		a11.addincident("SICK", 10000,new Date("2103-12-12"));
		System.out.println(a11.getincidentlist(new Date("2104-12-12")).get(0).getevent());
		System.out.println(Date.Stringinform(a11.getincidentlist().get(1).getdate()));
		System.out.println(a11.getprice()[0]+"  "+a11.getprice()[1]);
	}
}
