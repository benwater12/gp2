/**
 * @author B05505033 Benson Weng
 * @since 2018/06/23
 * @version 1.0
 <h1>This class helps others to use the Date without going through all the LocalDate class. In other words. Information concealment!</h1>
 <p>Change Log<br>
 1.0:the class is done and finished testing.

 */
package hotelmain;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.time.Period;

public class Date {
private	static final DateTimeFormatter format = DateTimeFormatter.ofPattern("yyyy-MM-dd");//this will be used a lot.
	private LocalDate tm;
	/**
	 * Wrapping class?
	 * @param a
	 * converstion Localdate
	 */
	Date(LocalDate a)
	{
		tm=a;
	}
	/**
	 * Wrapping class?
	 * @param a
	 * in the form yyyy-MM-dd, if not inputs today's date.
	 */
	public	Date(String a)
	{try {
		tm=getformatdate(a).tm;}
	catch(DateTimeParseException c)
	{
		tm=LocalDate.now();
	}
	}
	/**
	 * this give todays Date
	 */
public	Date()
	{
		tm=LocalDate.now();
	}
	/**
	 * this helps others to create a Date with the correct format, without reading all those Localdate notes
	 * @param a
	 * in the format yyyy-MM-dd 
	 * @return
	 * if correct it gives you yyyy-MM-dd Date.
	 * @throws
	 * wrong throws DateTimeParseException.
	 */
	public static Date  getformatdate(String a)throws DateTimeParseException
	{
		Date temp =new Date();
		temp.tm=LocalDate.parse(a, format);
		return temp;
		
	}
	/**
	 * this helps others to create a Date with the correct format, based on the Date+ int
	 * @param a
	 * how much days after(>0) or before(<0)
	 * @return
	 * the date with the asked difference
	 */
	public static Date  getformatdate(int day, Date date )throws DateTimeParseException
	{if(day>=0) {
		Date temp=new Date();
		temp.tm=date.tm.plusDays(day);
		return temp;}
	else {
		Date temp=new Date();
		temp.tm=date.tm.minusDays(day*-1);
		return temp;
	}
		
	}
	

	/**
	 * this helps others to format a LocalDate
	 * @return
	 * the asigned date formatter which is yyyy-MM-dd
	 */
	public static DateTimeFormatter  getformat()throws DateTimeParseException
{
	return format;}
	/**
	 * like to toString method, but this one is only in yyyy-MM-dd
	 * @param a
	 * toString class
	 * @return
	 * something in yyyy-MM-dd
	 */
	public static String Stringinform(Date a)
	{
		return a.tm.format(format);
		
		
	}
	/**
	 * A function which helps to calculate diffrence.(including first and last!)
	 * @param after
	 * Later Date
	 * @param before
	 * Before Date
	 * @return
	 * difference, if the order is wrong, will return the difference in the negative sign and have -2 error
	 */
	public static int difference(Date after,Date before)
	{
	int temp=0;	
	Period a=Period.between(before.tm, after.tm);
	temp=a.getDays()+a.getMonths()*30+a.getYears()*360+1;
		return temp;
	}
	/**
	 * NOT SUGGESTED.
	 * this is in case you want to us Date as A LocalDate
	 * @param a
	 * Date wanted to be trasfered
	 * @return
	 * A LocalDate(shallow copy)
	 */
	 static LocalDate toLocal(Date a)
	{
		return a.tm;
	}
	}
