
package hotelmain;

//import java.time.LocalDate;
import java.time.format.DateTimeParseException;
import java.util.Scanner;

/**
 * @author B05505033 Benson Weng
 * @since 2018/06/09
 * @version 1.0
 *          <h1>This class represents a event which cause extra expenses</h1>
 *          <p>
 * 			Change Log<br>
 *          0.1: Generated this class<br>
 *          0.2:the class is mostly done, and added some commits<br>
 *          1.0:the class is done thanks!
 */
public class Incident {
	private Date happen;
	private int price;
	private String account;

	/**
	 * A default which helps the user to input data. GIVEN UP
	 * 
	 * @deprecated Using Incident(String event, int price2,LocalDate happen).
	 *
	 */
 Incident(Scanner theevent) {
		try {
			String temp = theevent.nextLine();
			System.out.println("Input the event name" + " the date format should be yyyy-MM-dd");
			happen =new Date(temp);
		} catch (DateTimeParseException e) {
			System.out.println("the date wasn't inputed correctly, inputing as default which is today");
			happen = new Date();
			System.out.println(happen);
		}
		System.out.println("Input the event name");
		account = theevent.nextLine();
		System.out.println("Input the cost");
		try {
			price = Integer.parseInt(theevent.nextLine());
		} catch (NumberFormatException a1) {
			System.out.println("the cost wasn't an interger, inputing defalut cost as 1000.");
			price = 1000;
		} finally {
			theevent.close();
		}
	}

	 /**
	  * the suggested con
	 * @param event
	 * the name of the event
	 * @param price2
	 * total cost
	 * @param happen
	 * Date input
	 */
	Incident(String event, int price2, Date happen) {
		this.happen = happen;
		price = price2;
		account = event;
}

	/**
	 * as said this helps the user to change this incident price
	 * 
	 * @param a
	 *            the real price whic was inputed
	 * 
	 */
	void changeprice(int a) {
		this.price = a;
	}

	/**
	 * normal get function of price
	 * 
	 * @return int which means the price
	 * 
	 */
	public	int getprice() {
		return price;
	}

	/**
	 * normal get function which tell the name
	 * 
	 * @return String which contain the name
	 * 
	 */
	public	String getevent() {
		return new String(account);
	}

	/**
	 * normals get function which gives the date.
	 * 
	 * @return a LocalDate.
	 */
	public Date getdate() {
		Date temp=new Date(Date.Stringinform(happen));
		
		return temp;
	}

	/**
	 * testing main
	 * 
	 * @param a
	 */
	public static void main(String[] a) {
		new Incident(new Scanner(System.in));
	}
}
