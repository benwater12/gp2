/**
 * @author B05505033 Benson Weng
 * @since 2018/06/09
 * @version 1.0
 <h1>This class represents a hotel which manages all the rooms, including checkin, checkout...etc</h1>
 <p>Change Log<br>
 0.1: Generated this class<br>
 0.2:the class  done checkout method.<br>
 0.3:the class  done checkin and getroom method.<br>
 1.0:the class is done and finished testing.

 */
package hotelmain;
import java.time.Period;
import java.util.ArrayList;
import pet.*;
public class Hotel {
	private Room[] rooms=new Room[20];
	public Hotel(){
	for(int i=0;i<rooms.length;i++)
	rooms[i]=new Room();
	}
	/**
	 * this deletes the room out of data which is the room index!
	 * @param a
	 * In the form "int" plz, or it will say you are wrong.
	 * @return
	 * tells whether the operation is ok or not
	 */
	public boolean checkout(int room) {	
		class Wrongfloor extends Exception
		{
			private static final long serialVersionUID = 1L;

			Wrongfloor(int a)
			{
				super(new Integer(a).toString());
			}
		}
		try{
			int temroom=(room-1);
			if(temroom>20)
			{throw  new Wrongfloor(temroom);}
			else
			{
			rooms[temroom].getnoeat().clear();
			rooms[temroom].getincidentlist().clear();
			rooms[temroom].getnotice().clear();
			rooms[temroom]=new Room();
			return true;
			}
			}
		catch(NumberFormatException a)
		{
			System.out.println("error Incorrect format");
		}
		catch(Wrongfloor c)
		{
			String b;
			b=c.getMessage().substring(19);
			System.out.println(b+" does not exsist in the moment");
		}
		return false;
				
	}

/**
 * takes a pet in an register at a room, if all the rooms are full, tell the manager that full
 * @param pet
 * new pet, can be lizard,dog or cat
 * @param leave
 * the leave Date in yyyy-MM-dd, if not plz see the Room constuctor will be taken as 3 days after arrive
 * @param arrive
 * just a random date you can input.
 * @return
 * boolean  true as success false as failed to make a room
 */
public boolean checkin(Pet pet,String leave,Date arrive) {
	for(int i=0;i<rooms.length;i++)
{try {
	if(rooms[i].checkvacant())
	{
		System.out.println("ok");
	rooms[i]=new Room(pet,leave,arrive);
	System.out.println("the room number is "+(i+1));
	return true;
	}}
catch(NullPointerException a)
{
	a.printStackTrace();
	System.out.println("NG");
	continue;
}
	}
	return false;
}

/**
 * gets a room with the following index
 * @param i
 * from 1 to 20
 * @return
 * if it's not 1 to 20 return null, shallow copy notice. 
 */
public Room getRoom(int i) {
	if(i<1||i>20)
	{return null;}
	return rooms[(i-1)];
}
/**
 * @param ID
 * the serial number which was give when the order was placed
 * @return
 * int which tells the room number 1-20, -1 if nothing is found
 */
public int getroombyID(String ID)
{
	for(int i=0;i<rooms.length;i++)
	{
		try {
			if(rooms[i].match(ID))
			{
				return i+1;
			}
		}
		catch(NullPointerException a)
		{
			continue;
		}
	}	
return -1;}
/**
 * a function which takes masters name by input
 * @param master
 * the master's name(case sensitive)
 * @return
 * All the roomnumbers which the matching mastername.1-20
 */
public ArrayList<Integer> getroombyname(String master)
{
	ArrayList<Integer> temp=new ArrayList<Integer>();
	for(int i=0;i<rooms.length;i++)
	{
		try {
			if(rooms[i].matchmaster(master))
			{
				temp.add(i+1);
			}
			}
		catch(NullPointerException a)
		{
			continue;
		}
	}
	return temp;
}
/**
 * a function which takes pet's name by input
 * @param master
 * the pet's name(case sensitive)
 * @return
 * All the roomnumbers which the matching petname.1-20
 */
public ArrayList<Integer> getroombypet(String pet)
{
	ArrayList<Integer> temp=new ArrayList<Integer>();
	for(int i=0;i<rooms.length;i++)
	{
		try {
			if(rooms[i].matchpet(pet))
			{
				temp.add(i+1);
			}
			}
		catch(NullPointerException a)
		{
			continue;
		}
	}
	return temp;
}
/**
 * the difference of the stay. note including first and last day.
 * @param a
 * a room which need to be inspected
 * @return
 * int which are in days, it shouldn't be negative.
 */
public static int getstay(Room a)
{
	Period p=Period.between(a.arrive, a.leave);
	int a1=p.getDays()+p.getMonths()*30+p.getYears()*360+1;
	return a1; 
	}
/**
 * this it the place where the hotel wanted to input a incident in the asigned room.
 * * @param event
	 * the name of the event
	 * @param price2
	 * total cost
	 * @param happen
	 * Date input
 * @param room
 * room number assigning
 * 1-20
 */
public void incidenthappen(String happen,int price,Date event,int room)
{
	rooms[room-1].addincident(happen,price,event);
}
public static void main(String[] a)
{
	Hotel a1=new Hotel();

	
	Services[] a12= {Services.Extracam,Services.Walking};
Lizard a13	=new Lizard( "2019-10-10",  "12",  "24",a12  );
a13.addnoeat("killing?");
	System.out.println(a1.checkin(a13, "2102-03-02",new Date("2102-10-30")));
	System.out.println(a1.checkin(new Lizard( "12",  "12",  "12",a12  ), "2101-01-22",new Date("2102-2-17")));
	System.out.println(a1.getroombypet("12"));
	System.out.println(a1.getRoom(1).getID());
	System.out.println(a1.getroombyname("12"));
	System.out.println(Hotel.getstay(a1.getRoom(2)));
	System.out.println(a1.getRoom(1).getnoeat().get(0));
	System.out.println(a1.getroombyname("12"));
	System.out.println(a1.checkin(new Cat( "12",  "Al Dusty",  "Al Dusty",a12  ), "2101-01-22",new Date("2102-2-17")));
	System.out.println(a1.checkin(new Dog( "2102-12-12",  "Al Dusty",  "Al Dusty",a12  ), "2101-01-22",new Date("2102-2-17")));
	System.out.println(a1.getRoom(3).activeservices());
	System.out.println(a1.getroombyname("Al Dusty"));
	a1.getRoom(3).addincident("dead1", 120,new Date());
	a1.incidenthappen("???", 123, new Date(), 3);
	System.out.println(a1.getRoom(3).getincidentlist(new Date()).get(1).getevent());
	System.out.println(a1.getRoom(3).getprice()[0]);
	System.out.println(a1.getroombyname("Al Dusty"));
	
}
}

