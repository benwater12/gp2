package pet;
/**
 * @author B05505033 Benson Weng
 * @since 2018/06/9
 * @version 1.0
 <h1>This class represents a service</h1>
 <p>Change Log<br>
 0.1: Generated this class<br>
 0.2:thinking of services.<br>
 1.0: DONE?<br>
1.1 : added a constructor for futher use. 
 */

public enum Services {
	Walking(200), Extracam(400), Bettertoys(300), lightbulb(200),Catnip(200);
	 private final int price;//used the take in the price
	 private boolean active=false;//default
	 private Services(int price) {
		this.price=price;
		}
	 public int price() {
		return this.price; 
	 }
	/**Get the said services is active or not
	 * @return
	 * active or not.
	 */
	public boolean isActive() {
		return active;
	}
	/**
	 * @param active
	 * set the services as used or not.
	 */
	 void setActive(boolean active) {
		this.active = active;
	}
	 
}
