package pet;

import java.util.ArrayList;
import java.util.Scanner;

/**
 * @version 1.1
 * @author b05505033 Benson Weng
 * @since 2018/6/10
 *        <h1>a class for a Dog</h1>
 *        <p>
 * 		Change Log<br>
 *        0.1: made this class<br>
 *        0.2 : added services<br>
 *        0.3 major privact changes<br>
 *        1.0:Done<br>
 *        1.1 ADDed a consturctor for futher use(maybe GUI)
 */
public class Dog extends Pet {
	 private static Services[] serving = { Services.Bettertoys, Services.Extracam, Services.Walking };
	private String name;

	/**
	 * suggested form
	 * 
	 * @param birth
	 *            Pet birthday
	 * @param master
	 *            master name input
	 * @param name
	 *            pet name input
	 * @param active
	 *            the services which you want to activate. The input format should be in a array, dont worry, this is foolproof, if you inputted a services which didnt existed, it will ignore it. 
	 */
	public Dog(String birth, String master, String name, Services[] active) {
		super(birth, master);
		this.name = name;
		serving[0].setActive(false);
		for (int i = 0; i < active.length; i++) {
			
			for (int n = 0; n < serving.length; n++) {
				if (active[i].toString().equals(serving[n].toString())) {
					serving[n].setActive(true);
					System.out.println(serving[n]+" active");
				}
			}
		}
	}

	public Dog(String birth, String master, String name, Services[] active,ArrayList<String> noeat,ArrayList<String> notice) {
		super(birth, master,notice, noeat);
		this.name = name;
		serving[0].setActive(false);
		for (int i = 0; i < active.length; i++) {
			
			for (int n = 0; n < serving.length; n++) {
				if (active[i].toString().equals(serving[n].toString())) {
					//System.out.println("fukc?");
					serving[n].setActive(true);
					System.out.println(serving[n]+" active");
				}
			}
		}
	}
	/**
	 * @deprecated
	 */
	Dog() {
		super();
		Scanner event = new Scanner(System.in);
		System.out.println("Input the pet name");
		name = event.nextLine();
		System.out.println("Choose active service");
		System.out.println(getServices() + " use /n as escape");
		for (int i = 0; i > -1; i++) {

			String command = event.nextLine();
			try {
				int temp = Integer.parseInt(command);
				setServices(temp);
				System.out.println(serving[i].toString() + " active");
			} catch (NumberFormatException a) {
				if (command.equals("/n")) {
					break;
				}
				System.out.println("Error unknow command.");
			} catch (NullPointerException c) {
				System.out.println("Error unknown index.");
			} catch (ArrayIndexOutOfBoundsException a) {
				System.out.println("all services activted.");
				break;
			}

		}
		event.close();
	}

	/**
	 * get all services in the class
	 * @return
	 * a String with all the info<br>
	 *ex : 1. Bettertoys 2. XXXX 3. XXXX
	 */public static String getServices() {
		// TODO Auto-generated method stub
		return "1." + serving[0].toString() + " 2." + serving[1].toString() + " 3." + serving[2].toString();
	}

	@Override
	void setServices(int serve) {
		for (int i = 0; i < serving.length; i++) {
			if (serve == serving[i].ordinal()) {
				serving[i].setActive(true);
			}
		}
	}

	@Override
	public String getname() {
		return new String(name);
	}

	@Override
	public String getactive() {
		String store = " ";
		for (int i = 0; i < serving.length; i++) {
			if (serving[i].isActive()) {
				store = store + " " + serving[i].name() + " active now. ";
			}
		}
		store=store.substring(2, (store.length()-1));
		return store;
	}

	@Override
	public
	int getprice() {
		int temp = 0;
		for (int i = 0; i < serving.length; i++) {
			if (serving[i].isActive()) {
				temp = temp + serving[i].price();
			}
		}
		return temp;
	}

	@Override
	public boolean equals(Pet a,String petname) {
		if (a.getname().equals(name)&&petname.equals(this.master)&&a.birthday.isEqual(this.birthday))
			{return true;}
		return false;
	}
}
