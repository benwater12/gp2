/**
 * @version 1.4
 * @author b05505033 Benson Weng
 *@since 2018/6/10
 *<h1>a class for a Pet</h1>
 *<p>Change Log<br>
* 1.0 : finished all constructuors and set-up methods.<br>
* 1.1 : Done with Pet()<br>
* 1.2 : Fixed some issues that causes the whole thing the crash.<br>
* 1.3 : added some commits.<br>
* 1.4: major privact changes<br>
 */
package pet;

import java.util.ArrayList;
import java.util.Scanner;
import java.time.LocalDate;
import java.time.Period;
import java.time.format.DateTimeParseException;
import hotelmain.Date;

public abstract class Pet {
	ArrayList<String> noeat;//noeat String
	ArrayList<String> notice;
	LocalDate birthday;
	String master;
	/**
	 * this compares the master's name
	 * 
	 * @param a
	 *            the name of the master, this dont care the uppercase or lowercase
	 * @return a boolean tells you whether the master is the same
	 * 
	 */
	public boolean compair(String master) {
		if (master.toLowerCase().equals(this.master.toLowerCase()))
			return true;
		return false;
	}

	/**
	 * @param a
	 *            Date wanted to be inputed, suggested form yyyy-MM-dd
	 * @return boolean true=match, false=not match.
	 * @throws If
	 *             it's the wrong Date.getformat() throws DateTimeParseException as the date
	 *             couldnt be parse
	 */
	public boolean compairBd(String a) throws DateTimeParseException {
		try {
			if (a.equals(birthday.format(Date.getformat())))
				return true;
		} catch (DateTimeParseException a1) {
			throw a1;
		}
		return false;
	}

	/**
	 * gives the array of no eating
	 * 
	 * @return the original noeat array not a new one!!!
	 */
	public ArrayList<String> getnoeat() {
		return noeat;
	}

	/**
	 * gives the array of special list
	 * 
	 * @return the original notice array not a new one!!!
	 */
	public ArrayList<String> getnotice() {
		return notice;

	}

	/**
	 * @return Birthday in the form yyyy-MM-dd
	 */
	public String getBitrthday() {
		return Date.getformat().format(birthday);
	}

	/**
	 * this func uses a String to set up the new birthday, the date is after today, then print "Your pet is not born yet... Taking default input which is 2017-01-01"
	 * @param a
	 * a string in the Date.getformat() yyyy-MM-dd
	 * @throws
	 * if the date is not in the yyyy-MM-dd Date.getformat().
	 * 
	 */
 private void setBirthday(String birthday) throws DateTimeParseException {
		try {
			if (LocalDate.parse(birthday, Date.getformat()).isAfter(LocalDate.now())) {
				System.out.println("Your pet is not born yet... Taking default input which is 2017-01-01");
				this.birthday=LocalDate.parse("2017-01-01", Date.getformat());
			}
			else {
			this.birthday = LocalDate.parse(birthday, Date.getformat());}
		} catch (DateTimeParseException b) {
			throw b;
		}

	}

	/**
	 * this helps the user to know the age of the pet.
	 * @return this gives a String which a age in "age : XX years XX months"
	 * 
	 */
	public String getage() {
		String a;
		LocalDate today = LocalDate.now();
		System.out.println("Today : " + today);
		LocalDate birthDate = LocalDate.of(birthday.getYear(), birthday.getMonth(), birthday.getDayOfMonth());
		System.out.println("BirthDate : " + birthday.format(Date.getformat()));
		Period p = Period.between(birthDate, today);
		a = "age : " + p.getYears() + " years " + p.getMonths() + " months";
		return a;
	}

	/**
	 * @return
	 * nothing special, it's only a getter for the master's name(Deep copy)
	 */
	public String getMaster() {
		return new String(master);
	}


	/**
	 * addnotice:  add a extra notice to the array
	 * @param a
	 * the notice which desired to be inputed
	 *           
	 */
	public void addnotice(String input) {
		notice.add(input);
	}

	/**
	 *            add a extra noeat to the array
	 * @param a
	 * 
	 *            the noeat which desired to be inputed
	 */
	public void addnoeat(String input) {
		noeat.add(input);
	}

	/**
	 * remove one notice from the array of notice
	 * 
	 * @param a
	 *            remove a notice form the array must be the exact same typing.
	 * @param remove
	 * true for removes the last inputed, false for no touching anything if no match is found.
	 * @return a boolean tells you the removal is successful or not, if it's false, then it failed
	 */
	public boolean removenotice(String input, boolean remove) {
		boolean valid = notice.remove(input);
		if (valid) {
			return valid;
		} else {
			if (noeat.isEmpty())
				return false;
			if(remove) 
			{
				noeat.remove(noeat.size() - 1);
				return true;
			}
			else {
				return false;
			}
		}
	}

	/**
	 * remove one notice from the array of noeat
	 * 
	 * @param a
	 *           remove a noeat form the array must be the exact same typing.
	 @param remove
	 * true for removes the last inputed, false for no touching anything if no match is found.
	 * @return a boolean tells you the removal is successful or not, if it's false, then it failed
	 */
	public boolean removenoeat(String input, boolean remove) {
		boolean valid = noeat.remove(input);
		if (valid) {
			return valid;
		} else {
			if (noeat.isEmpty())
				return false;
			if(remove) 
			{
				noeat.remove(noeat.size() - 1);
				return true;
			}
			else {
				return false;
			}
		}
	}

	
	/**
	 * Setting services
	 * @param serve
	 * this helps to setup the services to active!<br> 
	 * to to be breif, please use the static method(GetServices()) in the Cat, Dog,or Lizard to check the list of services
	 */
	abstract void setServices(int serve);
	/**
	 * Just a get  pet name
	 * @return
	 * the name of the pet
	 */
	abstract public String getname();
	/**
	 * active services in a String!
	 * @return
	 * gives the a list of active services. it's in the form of<br> 
	 * Extracam active now.  Walking active now.
	 */
	public abstract String getactive();

	/**
	 * @return
	 * the total price of the one day SERVICES!!!
	 */
	abstract public  int getprice();

	/**
	 @param birth
	 * birthdat input in the form yyyy-MM-dd, default is set as 2017-01-01
	 * @param master
	 * the master name please notice that it's case sensitive.
	 * @param noeat
	 * well if you dont want to input noeat later, you can put it in here.
	 * @param notice
	  * well if you dont want to input notice later, you can put it in here.
	 */
	public Pet(String birth,String master,ArrayList<String> noeat,ArrayList<String> notice) {
		this.noeat=noeat;
		this.notice=notice;
		try {
			setBirthday(birth);
		} catch (DateTimeParseException a) {
			System.out.println("the date is incorrect format, inputing default as 2017-01-01");
			birthday = LocalDate.parse("2017-01-01", Date.getformat());
		}
		this.master=master;
	}
	/**
	 * the new constructor which should be used.
	 * @param birth
	 * birthdat input in the form yyyy-MM-dd, default is setted as 2017-01-01
	 * @param master
	 * the master name please notice that it's case sensitive.
	 */
	public Pet(String birth,String master) {
		noeat = new ArrayList<String>();
		notice = new ArrayList<String>();
		try {
			setBirthday(birth);
		} catch (DateTimeParseException a) {
			System.out.println("the date is incorrect, inputing default as 2017-01-01");
			birthday = LocalDate.parse("2017-01-01", Date.getformat());
		}
		this.master=master;
	}
	/**
	 * @deprecated
	 * Replaced by the Pet(String,String).
	 */
	public Pet() {
		noeat = new ArrayList<String>();
		notice = new ArrayList<String>();
	@SuppressWarnings("resource")
	Scanner	event = new Scanner(System.in);
		System.out.println("Input the pet birthday" + " the date Date.getformat() should be yyyy-MM-dd");
		try {
			birthday = LocalDate.parse(event.nextLine(), Date.getformat());
		} catch (DateTimeParseException a) {
			System.out.println("the date Date.getformat() is incorrect, inputing default as 2017-01-01");
			birthday = LocalDate.parse("2017-01-01", Date.getformat());
		}
		System.out.println("Input the master name");
		master = event.nextLine();
		System.out.println("Input notices, press /n to leave");
		for (int i = 0; i > -1; i++) {
			try {
				String e = event.nextLine();
				if (e.equals("/n")) {
					break;
				}
				noeat.add(e);
			} catch (NullPointerException a) {

				continue;
			}
		}
		System.out.println("Input noeats, press /n to leave");
		for (int b = 0; b > -1; b++) {

			try {
				String e = event.nextLine();
				if (e.equals("/n")) {
					break;
				}

				noeat.add(e);

			} catch (NullPointerException a) {

				continue;
			}

		}
	}
/**
 * It mathes name+mastername+age
 * @param pet
 * a Pet class, which has all the information
 * @param petname
 * nothing to say about this.
 * @return
 * * It mathes name, mastername, age, all must be the same in order to return true, otherwise false
 * 
 */
abstract public	boolean equals(Pet pet, String petname);

	public static void main(String[] a) {
		Pet a1 = new Lizard();
		a1.getage();
		System.out.println(a1.getMaster());
		System.out.println(a1.getnoeat().get(0));
		System.out.println(a1.getnotice().get(0));
		a1.setBirthday("2001-12-12");
		a1.setBirthday("2021-12-12");
		System.out.println(a1.getBitrthday());
		System.out.println(a1.getClass().getSimpleName());
		System.out.println(a1.getactive());

	}
}
