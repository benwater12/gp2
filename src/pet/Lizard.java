package pet;

import java.util.Scanner;

/**
 * @version 1.1
 * @author b05505033 Benson Weng
 * @since 2018/6/10
 *        <h1>a class for a Lizard</h1>
 *        <p>
 *        Change Log<br>
 *        0.1: made this class<br>
 *        0.2 : added services<br>
 *        0.3 major privact changes<br>
 *        1.0 Done<br>
 *        1.1 ADDed a consturctor for futher use(maybe GUI)
 */
public class Lizard extends Pet {
	private static Services[] a = { Services.Extracam, Services.Bettertoys, Services.lightbulb };
	private String name;

	/**
	 * suggested form
	 * @param birth
	 * Pet birthday
	 * @param master
	 * master name input
	 * @param name
	 * pet name input
	 * @param active
	 * the services which you want to activate. The input format should be in a array, dont worry, this is foolproof, if you inputted a services which didnt existed, it will ignore it. 
	 */
	public Lizard(String birth, String master, String name, Services[] active) {
		super(birth, master);
		this.name = name;
		for (int i = 0; i < active.length; i++) {
			
			for (int n = 0; n < a.length; n++) {
				if (active[i].toString().equals(a[n].toString())) {
					a[n].setActive(true);
					System.out.println(a[n]+" active");
				}
			}
		}
	}

	/**
	 * 
	 * only for one purpose use it has bugs on conditions
	 */
	@SuppressWarnings("deprecation")
	public Lizard() {
		super();

		Scanner event = new Scanner(System.in);
		System.out.println("Input the pet name");
		name = event.nextLine();
		System.out.println("Choose active service");
		System.out.println(getServices() + " use /n as escape");
		for (int i = 0; i < a.length; i++) {

			String command = event.nextLine();
			try {
				int temp = Integer.parseInt(command);
				setServices(temp);
				System.out.println(a[i].toString() + " active");
			} catch (NumberFormatException a) {
				if (command.equals("/n")) {
					break;
				}
				System.out.println("Error unknow command.");
			} catch (NullPointerException c) {
				System.out.println("Error unknown index.");
			} catch (ArrayIndexOutOfBoundsException a) {
				System.out.println("all services activted.");
				break;
			}

		}
		event.close();
	}

	/**
	 * get all services in the class
	 * @return
	 * a String with all the info
	 */
	public static String getServices() {
		// TODO Auto-generated method stub
		return "1." + a[0].toString() + " 2." + a[1].toString() + " 3." + a[2].toString();
	}

	@Override
	void setServices(int serve) {
		for (int i = 0; i < a.length; i++) {
			if (serve == a[i].ordinal()) {
				a[i].setActive(true);
			}
		}
	}

	@Override
	public String getname() {
		return new String(name);
	}

	@Override
	public String getactive() {
		String store = " ";
		for (int i = 0; i < a.length; i++) {
			if (a[i].isActive()) {
				store = store + " " + a[i].name() + " active now. ";
			}
		}
		store=store.substring(2, (store.length()-1));
		return store;
	}

	public int getprice() {
		int temp = 0;
		for (int i = 0; i < a.length; i++) {
			if (a[i].isActive()) {
				temp = temp + a[i].price();
			}
		}
		return temp;
	}

	@Override
	public boolean equals(Pet a,String petname) {
		if (a.getname().equals(name)&&petname.equals(this.master)&&a.birthday.isEqual(this.birthday))
			{return true;}
		return false;
	}
}
