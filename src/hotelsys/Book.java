package hotelsys;

import java.util.*;
import hotelmain.Date;
import hotelmain.*;
import pet.*;
/**
 * <h1>Book</h1>
 * <p>This Book class is the main system of the program.
 * @author b05505001
 * @version 1.0
 * @since 2018-06-28
 */
public class Book {
	Hotel hotel = new Hotel();
	/**
	 * Let the user choose the function he/she want to use.
	 */
	public void getAccess() {
		Scanner keyboard = new Scanner(System.in);
		boolean access = true;
		System.out.println("  \\Welcome to the Pet Hotel Booking System!/");
		while (access) {
			System.out.println("===============================================");
			System.out.println("Please choose the service you want.(*‘ω‘ *)");
			System.out.print("1.Make A Book. | 2.Booking History. | 3.Cancel A Book. | 4.Total Price. | 5.Leave.\nAction:");
			int act = keyboard.nextInt();
			switch (act) {
			case 1:
				try {
					makeABook(keyboard);
				} 
				catch (BookingException e) {
					System.out.println("==========\\\\ >>>>>>  FAIL   <<<<<< //==========");
					System.out.println("Sorry!"+e.getMessage());
				}
				break;
			case 2:
				try {
					bookingHistory(keyboard);
				} 
				catch (BookingException e) {
					System.out.println("==========\\\\ >>>>>>  FAIL   <<<<<< //==========");
					System.out.println("Sorry!"+e.getMessage());
				}
				break;
			case 3:
				try {
					cacelABook(keyboard);
				} catch (BookingException e) {
					System.out.println("==========\\\\ >>>>>>  FAIL   <<<<<< //==========");
					System.out.println("Sorry!"+e.getMessage());
				}
				break;
			case 4:
				System.out.println("Sorry!This fucnction is not finish yet.");
				try {
					getTotalPrice(keyboard);
				} catch (BookingException e) {
					System.out.println("==========\\\\ >>>>>>  FAIL   <<<<<< //==========");
					System.out.println("Sorry!"+e.getMessage());
				}
				break;
			case 5:
				System.out.println("======\\ May the wind carry your wings! /======");
				access = false;
				break;
			case 0:
				administration(keyboard);
				break;
			default:
				System.out.println("May the wind carry wings that are lost!");
				break;
			}
		}
	}
	/**
	 * This method will help the user to make a book.
	 * @param keyboard The Scanner that let the user to input data.
	 * @throws BookingException When some exceptions happen during the
	 * booking process,stop the system and show the error message.For
	 * Example:"There isn't enough room for you!"
	 */
	private void makeABook(Scanner keyboard) throws BookingException {
		System.out.println("<=============== Make A Book. ================>");
		Pet yourPet = inputPetProfile(keyboard);
		System.out.println("Please choose the date you want to check in:");
		String checkin = keyboard.next();
		System.out.println("The date to check in:"+checkin);
		System.out.println("Please choose the date you want to check out:");
		String checkout = keyboard.next();
		System.out.println("The date to check out:"+checkout);
		if(!hotel.checkin(yourPet, checkout, new Date(checkin))) {
			throw new BookingException("There isn't enough room for you!");
		}
		System.out.println("Success!");
	}
	/**
	 * This method will help the user to input the pet's data like its Master name,
	 * its name,its birthday etc.Finally it will return a pet object to the makeABook
	 * method to let it do the next process.
	 * @param keyboard The Scanner that let the user to input data.
	 * @return A pet object that created by the data that are input by the user.
	 * @throws BookingException When some exceptions happen during the
	 * booking process,stop the system and show the error message.For
	 * Example:"We didn't offer the service for such kind of pet!"
	 */
	private Pet inputPetProfile(Scanner keyboard) throws BookingException {
		Pet yourPet;
		System.out.println("Please tell me your name:");
		String name = keyboard.next();
		System.out.println("Your name:"+name);
		System.out.println("Please tell me your pet's name:");
		String petName = keyboard.next();
		System.out.println("Your pet's name:"+petName);
		System.out.println("Please tell me your pet's birthday:");
		String petBirth = keyboard.next();
		System.out.println("Your pet's birthday:"+petBirth);
		System.out.println("Please choose the type of your pet:");
		System.out.print("1. Dog 2. Cat 3. Lizard.\nAnwser:");
		int petType = keyboard.nextInt();
		switch(petType) {
		case 1:
			yourPet = new Dog(petBirth, name, petName, chooseServices(keyboard));
			break;
		case 2:
			yourPet = new Cat(petBirth, name, petName, chooseServices(keyboard));
			break;
		case 3:
			yourPet = new Lizard(petBirth, name, petName, chooseServices(keyboard));
			break;
		default:
			throw new BookingException("We didn't offer the service for such kind of pet!");
		}
		System.out.print("Do you want to add some notices?\n1.Yes 2.No:");
		if(keyboard.nextInt()==1) {
			System.out.println("Is there anything your pet could not eat?");
			System.out.print("1.Yes 2.No:");
			if(keyboard.nextInt()==1) {
				System.out.print("The number of food could not eat:");
				int numOfNotEat = keyboard.nextInt();
				for(int i=0;i<numOfNotEat;i++) {
					System.out.print("The food:");
					String food = keyboard.next();
					yourPet.addnoeat(food);
				}
				System.out.println(yourPet.getnoeat());
			}
			System.out.print("Any other notices?\n1.Yes 2.No:");
			if(keyboard.nextInt()==1) {
				System.out.print("The number of notices:");
				int numOfNotice = keyboard.nextInt();
				for(int i=0;i<numOfNotice;i++) {
					System.out.print("The "+(i+1)+".notices:");
					String notice = keyboard.next();
					yourPet.addnotice(notice);
				}
				System.out.println(yourPet.getnotice());
			}
		}
		return yourPet;
	}
	/**
	 * This method will help the user to choose the services he/she want from 
	 * the Services{Walking,Extracam,Bettertoys,lightbulb,Catnip}.Finally it
	 * will return an array of Services that include the services choose by the 
	 * user.
	 * @param keyboard The Scanner that let the user to input data.
	 * @return An array of Services that include the services choose by the 
	 * user.
	 */
	private Services[] chooseServices(Scanner keyboard) {
		ArrayList<Services> services = new ArrayList<Services>();
		System.out.println("Please choose the services you want.[1.Yes 2.No]");
		System.out.print("1.Walking:");
		if(keyboard.nextInt()==1)services.add(Services.Walking);
		System.out.print("2.Extracam:");
		if(keyboard.nextInt()==1)services.add(Services.Extracam);
		System.out.print("3.Bettertoys:");
		if(keyboard.nextInt()==1)services.add(Services.Bettertoys);
		System.out.print("4.lightbulb:");
		if(keyboard.nextInt()==1)services.add(Services.lightbulb);
		System.out.print("5.Catnip:");
		if(keyboard.nextInt()==1)services.add(Services.Catnip);
		Services[] temp = new Services[services.size()];
		for(int i=0;i<services.size();i++) {
			temp[i] = services.get(i);
		}
		return temp;
	}
	/**
	 * This method will help the user to check the booking history and find he/she own 
	 * book.
	 * @param keyboard The Scanner that let the user to input data.
	 * @throws BookingException When some exceptions happen during the booking process,
	 * stop the system and show the error message.For Example:"Sorry!We didn't find 
	 * your book!"
	 */
	private void bookingHistory(Scanner keyboard) throws BookingException {
		System.out.println("<============= Booking History. ==============>");
		System.out.println("How do you want to check the book?");
		System.out.print("1.By reservation ID 2.By your pet's name:");
		int checkMethod = keyboard.nextInt();
		switch(checkMethod) {
		case 1:
			System.out.println("Please tell me your reservation ID:");
			int roomNo = hotel.getroombyID(keyboard.next());
			if(roomNo<0)throw new BookingException("We didn't find your book!");
			checkRoomInfo(roomNo);
			break;
		case 2:
			System.out.println("Please tell me your pet's name:");
			String petName = keyboard.next();
			System.out.println("Your pet's name:"+petName);
			ArrayList<Integer> prob = hotel.getroombypet(petName);
			if(prob.isEmpty())throw new BookingException("We didn't find your book!");
			for(int i=0;i<prob.size();i++) {
				checkRoomInfo(prob.get(i));
				System.out.print("Is this your book?\n1.Yes 2.No:");
				if(keyboard.nextInt()==1) {
					System.out.println("Your reservation ID:"+hotel.getRoom(prob.get(i)).getID());
					break;
				}
				else {
					if(i==(prob.size()-1)) {
						throw new BookingException("We didn't find your book!");
					}
				}
			}
			break;
		default:
			throw new BookingException("We didn't offer such kind of inquiry method!");
		}
	}
	/**
	 * This method will help the bookingHistory method to check the informations
	 * of a Room by the room number which is match by the pet's name or booking ID
	 * input by the user.
	 * @param roomNo The room number which is match by the pet's name or booking ID
	 * input by the user.
	 */
	private void checkRoomInfo(int roomNo) {
		Room room = hotel.getRoom(roomNo);
		System.out.println("Room number:"+roomNo);
		System.out.println(room.getPetInfo());
		System.out.println("Date arrive:"+Date.Stringinform(room.getarrivaldate()));
		System.out.println("Date leave:"+Date.Stringinform(room.getleavedate()));
	}
	/**
	 * This method will help the user to cancel a book from the System.If the book doesn't
	 * exist,it will throw a BookingException and show the reason why it will fail.
	 * @param keyboard The Scanner that let the user to input data.
	 * @throws BookingException When some exceptions happen during the booking process,
	 * stop the system and show the error message.For Example:"Your book doesn't exist."
	 */
	private void cacelABook(Scanner keyboard) throws BookingException {
		System.out.println("<============== Cancel A Book. ===============>");
		System.out.println("Please tell me your reservation ID:");
		int roomNo = hotel.getroombyID(keyboard.next());
		if(roomNo<0)throw new BookingException("Your book doesn't exist.");
		hotel.checkout(roomNo);
		System.out.println("Success!");
	}
	/**
	 * This method will help the user to find the total price he/she should pay for
	 * their pet.
	 * @param keyboard The Scanner that let the user to input data.
	 * @throws BookingException When some exceptions happen during the booking process,
	 * stop the system and show the error message.For Example:"Your book doesn't exist."
	 */
	private void getTotalPrice(Scanner keyboard) throws BookingException {
		System.out.println("<=============== Total Price. ================>");
		System.out.println("Please tell me your reservation ID:");
		int roomNo = hotel.getroombyID(keyboard.next());
		if(roomNo<0)throw new BookingException("Your book doesn't exist.");
		Room room = hotel.getRoom(roomNo);
		int price = 0;
		for(int i=0;i<Hotel.getstay(room);i++) {
			for(int e:room.getprice()) {
				price += e;
			}
		}
		System.out.println("Your total price is:"+price);
	}
	/**
	 * This method is especially access by the administrator to add the incident
	 * to the room that incident happened.
	 * @param keyboard The Scanner that let the administrator to input data.
	 */
	private void administration(Scanner keyboard) {
		System.out.println("=====// <<<<<<  Administration   >>>>>> \\\\=====");
		System.out.println("The room that accident happened:");
		int accid = keyboard.nextInt();
		if(accid!=0) {
			System.out.println("The accident prescription:");
			String event = keyboard.next();
			System.out.println("The cost to fix the accident:");
			int price = keyboard.nextInt();
			System.out.println("The date that accident happened:");
			String date = keyboard.next();
			hotel.getRoom(accid).addincident(event, price, new Date(date));
		}
	}
	/**
	 * The main process system.
	 * @param args The parameters.
	 */
	public static void main(String[] args) {
		System.out.println("          ========System Start========\n");
		Book book = new Book();
		book.getAccess();
		System.out.println("\n          =========System End=========");
	}
}
